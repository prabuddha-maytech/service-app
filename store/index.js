const Cookie = process.client ? require('js-cookie') : undefined
const cookieparser = process.server ? require('cookieparser') : undefined;

const API_BASE_PATH = "/api/v1/";

export const state = () => {
  return {
    auth: null,
    totalGridItems: 0,
    numberOfPages: 0,
    customers: [],
    vehicles: [],
    images: [],
    categories: [],
    inventory: [],
    services: [],
    technicians: [],
    jobs: [],
    spares: [],
    jobId: '',
    spareTotal: 0,
    jobServices : [],
    jobServiceTotal: 0,
    jobDiscount: 0,
  }
}
export const mutations = {
  // auth mutations
  setAuth (state, auth) {
    state.auth = auth
  },
  unSetAuth(state, auth) {
    state.auth = null;
  },

  //page mutations
  resetTotalItems(state, totalItems){
    state.totalGridItems = totalItems
  },
  setTotalItems(state, totlaItems) {
    state.totalGridItems = totlaItems

  },

  resetNumberOfPages(state, numberOfPages){
    state.numberOfPages = numberOfPages
  },
  setNumberOfPages(state, numberOfPages) {
    state.numberOfPages = numberOfPages

  },

  resetPagination(state, item){
    state.totalGridItems = 0;
    state.numberOfPages = 0;
  },

  //customer mutations
  resetCustomers(state, customerList){
    state.customers = customerList
  },
  setCustomers(state, customerList) {
    state.customers.push(customerList);
  },
  addNewCustomer(state, newCustomer){
    state.customers.unshift(newCustomer)
  },
  updateCustomer(state, editCustomer){
    let editIndex = state.customers.findIndex(item => item.id == editCustomer.id)
    Object.assign(state.customers[editIndex], editCustomer)
  },
  deleteCustomer(state, customerId){
    let editIndex = state.customers.findIndex(item => item.id == customerId)
    state.customers.splice(editIndex, 1)
  },

  //image mutations
  resetImages(state, imageList){
    state.images = imageList
  },
  setImage(state, imageList) {
    state.images.push(imageList)
  },

  //vehicle mutations
  resetVehicles(state, vehicleList){
    state.vehicles = vehicleList;
  },
  setVehicles(state, vehicleList) {
    state.vehicles.push(vehicleList);
  },
  addNewVehicle(state, newVehicle){
    state.vehicles.unshift(newVehicle)
  },
  updateVehicle(state, editVehicle){
    let editIndex = state.vehicles.findIndex(item => item.id == editVehicle.id)
    Object.assign(state.vehicles[editIndex], editVehicle)
  },
  deleteVehicle(state, vehicleId){
    let editIndex = state.vehicles.findIndex(item => item.id == vehicleId)
    state.vehicles.splice(editIndex, 1)
  },
  
  //Category Mutations
  resetCategories(state, categoryList){
    state.categories = categoryList;
  },
  setCategories(state, categoryList) {
    state.categories.push(categoryList);
  },
  addNewCategory(state, newCategory){
    state.categories.push(newCategory)
  },
  updateCategory(state, editCategory){
    let editIndex = state.categories.findIndex(item => item.id == editCategory.id)
    Object.assign(state.categories[editIndex], editCategory)
  },
  deleteCategory(state, categoryId){
    let editIndex = state.categories.findIndex(item => item.id == categoryId)
    state.categories.splice(editIndex, 1)
  },

  //Inventory Mutations
  resetInventory(state, inventoryList){
    state.inventory = inventoryList;
  },
  setInventory(state, inventoryList) {
    state.inventory.push(inventoryList);
  },
  addNewInventory(state, newInventory){
    state.inventory.unshift(newInventory)
  },
  updateInventory(state, editInventory){
    let editIndex = state.inventory.findIndex(item => item.id == editInventory.id)
    Object.assign(state.inventory[editIndex], editInventory)
  },
  deleteInventory(state, inventoryId){
    let editIndex = state.inventory.findIndex(item => item.id == inventoryId)
    state.inventory.splice(editIndex, 1)
  },

  //Service Mutations
  resetService(state, serviceList){
    state.services = serviceList;
  },
  setService(state, serviceList) {
    state.services.push(serviceList);
  },
  addNewService(state, newService){
    state.services.unshift(newService)
  },
  updateService(state, editService){
    let editIndex = state.services.findIndex(item => item.id == editService.id)
    Object.assign(state.services[editIndex], editService)
  },
  deleteService(state, ServiceId){
    let editIndex = state.services.findIndex(item => item.id == ServiceId)
    state.services.splice(editIndex, 1)
  },

  //Technician Mutations
  resetTechnician(state, technicianList){
    state.technicians = technicianList;
  },
  setTechnician(state, technicianList) {
    state.technicians.push(technicianList);
  },
  addNewTechnician(state, newTechnician){
    state.technicians.unshift(newTechnician)
  },
  updateTechnician(state, editTechnician){
    let editIndex = state.technicians.findIndex(item => item.id == editTechnician.id)
    Object.assign(state.technicians[editIndex], editTechnician)
  },
  deleteTechnician(state, technicianId){
    let editIndex = state.technicians.findIndex(item => item.id == technicianId)
    state.technicians.splice(editIndex, 1)
  },

  //Job Mutations
  resetJobs(state, jobList){
    state.jobs = jobList
  },
  setJobs(state, jobList) {
    state.jobs.push(jobList);
  },
  setJobId(state, jobId){
    state.jobId = jobId;
  },
  resetJobId(state, jobId){
    state.jobId = '';
  },
  deleteJob(state, jobId){
    let editIndex = state.jobs.findIndex(item => item.id == jobId)
    state.jobs.splice(editIndex, 1)
  },

  //Spare Mutations
  resetSpare(state, spareList){
    state.spares = spareList;
  },
  setSpare(state, spare) {
    state.spares.push(spare);
  },
  addNewSpare(state, newSpare){
    state.spares.push(newSpare)
  },

  calculateSparesTotal(state, spare){
    let spareTotal = state.spares.reduce((sum, item) => sum + item.total, 0);
    state.spareTotal = spareTotal.toFixed(2)
  },
  resetSpareTotal(state, spare){
    state.spareTotal = 0
  },

  //Spare Mutations
  resetJobService(state, jobServiceList){
    state.jobServices = jobServiceList;
  },
  setJobService(state, jobService) {
    state.jobServices.push(jobService);
  },
  addNewJobService(state, newJobService){
    state.JobServices.push(newJobService)
  },

  calculateJobServiceTotal(state, jobService){
    let jobServiceTotal = state.jobServices.reduce((sum, item) => sum + item.total, 0);
    state.jobServiceTotal = jobServiceTotal.toFixed(2);
  },
  resetJobServiceTotal(state, jobService){
    state.jobServiceTotal = 0
  },

  //Discuont Mutations
  setDiscount(state, discount){
    state.jobDiscount = discount;
  },

  resetDiscount(state, discount){
    state.jobDiscount = 0;
  },


}
export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    let auth = null
    if (req.headers.cookie) {
      const parsed = cookieparser.parse(req.headers.cookie)
      try {
        auth = JSON.parse(parsed.auth)
      } catch (err) {
        // No valid cookie found
      }
    }
    commit('setAuth', auth)
  },

  async logout({state, commit}){
    Cookie.remove('auth');
    commit('unSetAuth', null);
    this.$toast.error('Loggin Out').goAway(1500);
  },
  async showError({state, commit}, error){
    const errorMessage = error.data.error != null ? (typeof error.data.error[0] == 'string' ? error.data.error[0] :'Something went wrong' ) : 'Something went wrong';
    this.$toast.error(errorMessage).goAway(1500);
  },
  async connectionError({state, commit}, error){
    this.$toast.error(error).goAway(1500);
  },

  // Customer Actions //
  async getCustomers({state,commit}, options){
        const { page, itemsPerPage, sortBy, sortDesc } = options.options;
        const search = options.search;
        const sort = sortDesc[0] ? 'DESC' : 'ASC'
        const pageNumber = page - 1;
        const sortByValue = sortBy.length > 0 ? sortBy[0] : "id";
        console.log("sortby", sortBy);
        console.log("sortby", sortBy.length > 0);
        console.log("sortby", JSON.parse(JSON.stringify(sortBy)));
        console.log("page", page);
        commit('resetCustomers', []);
        commit('resetPagination', []);
        await this.$axios.$get(API_BASE_PATH +'customer/grid?pageNumber='+ pageNumber +'&sortBy='+ sortByValue +'&sortDirection='+ sort +'&pageSize=' + itemsPerPage)
          .then(res => {
              console.log("res", res);
              const customerRes = res.content != null ? res.content : [];
              const customerCount = res.size != null ? res.size : 0;
              const numberOfPages = Math.round(customerCount/ itemsPerPage);
              commit('setTotalItems', customerCount);
              commit('setNumberOfPages', numberOfPages )
              if(customerRes.length > 0){              
                customerRes.forEach(item => {
                  commit('setCustomers', item);
                })
              }
          })
    },
  async getCustomerList({state,commit}){
        // if(state.customers.length) return;
        await this.$axios.$get(API_BASE_PATH +'customer/list')
          .then(res => {
              const categoryRes = res.customers != null ? res.customers : [];
              if(categoryRes.length > 0){
                commit('resetCustomers', []);
                categoryRes.forEach(item => {
                  commit('setCustomers', item);
                })
              }
          })
    },
  async saveCustomer({commit}, newCostomer){
    await this.$axios.$post(API_BASE_PATH +'customer/', {
            ...newCostomer
          })
          .then(res => {
            console.log("res", res);
              const customerRes = res != null ? res : null;
              if(customerRes != null){
                commit('addNewCustomer', customerRes);
                this.$toast.success('Customer Added').goAway(1500); 
              }
          })
  },
  async updateCustomer({commit}, editCustomer,){
    const editCustomerObject = editCustomer.editCustomer;
    await this.$axios.$put(API_BASE_PATH +'customer/' + editCustomer.customerId, {
            ...editCustomerObject
          })
          .then(res => {
              const customerRes = res != null ? res : null;
              if(customerRes != null){
                commit('updateCustomer', customerRes);
                this.$toast.success('Customer Updated').goAway(1500); 
              }
          })
  },
  async deleteCustomer({commit}, customerId){
    await this.$axios.$delete(API_BASE_PATH +'customer/' + customerId)
          .then(res => {
            commit('deleteCustomer', customerId);
                this.$toast.success('Customer Deleted').goAway(1500);
              // const customerRes = res.data != null ? res.data : null;
              // if(customerRes != null){
              //   commit('deleteCustomer', customerId);
              //   this.$toast.success('Customer Deleted').goAway(1500); 
              // }
          })
  },

  // End Customer Actions //

  //Vehicle Actions
  async getVehicles({state,commit}, options){
        const { page, itemsPerPage, sortBy, sortDesc } = options.options;
        const search = options.search;
        const sort = sortDesc[0] ? 'DESC' : 'ASC'
        const pageNumber = page - 1;
        const sortByValue = sortBy.length > 0 ? sortBy[0] : "id";
        console.log("page", page);
        commit('resetVehicles', []);
        commit('resetPagination', []);
        await this.$axios.$get(API_BASE_PATH +'vehicle/grid?pageNumber='+ pageNumber +'&sortBy='+ sortByValue +'&sortDirection='+ sort +'&pageSize=' + itemsPerPage)
          .then(res => {
              const vehicleRes = res.content != null ? res.content : [];
              const vehicleCount = res.size != null ? res.size : 0;
              const numberOfPages = Math.round(vehicleCount/ itemsPerPage);
              commit('setTotalItems', vehicleCount);
              commit('setNumberOfPages', numberOfPages )
              if(vehicleRes.length > 0){              
                vehicleRes.forEach(item => {
                  commit('setVehicles', item);
                })
              }
          })
    },
     async getVehicleList({state,commit}){
        // if(state.customers.length) return;
        await this.$axios.$get(API_BASE_PATH +'vehicle/list')
          .then(res => {
              const vehicleRes = res.vehicles != null ? res.vehicles : [];
              if(vehicleRes.length > 0){
                commit('resetVehicles', []);
                vehicleRes.forEach(item => {
                  commit('setVehicles', item);
                })
              }
          })
    },
     async findVehicleByComapny({state,commit}, customerId){
        // if(state.customers.length) return;
        await this.$axios.$get(API_BASE_PATH +'vehicle/findByCustomer/' + customerId)
          .then(res => {
              const vehicleRes = res.vehicles != null ? res.vehicles : [];
              if(vehicleRes.length > 0){
                commit('resetVehicles', []);
                vehicleRes.forEach(item => {
                  commit('setVehicles', item);
                })
              }
          })
    },

  async saveVehicle({commit}, newVehicle){
  await this.$axios.$post(API_BASE_PATH + 'vehicle', {
          ...newVehicle
        })
        .then(res => {
            const customerRes = res != null ? res : null;
            if(customerRes != null){
              commit('addNewVehicle', customerRes);
              this.$toast.success('Vehicle Added').goAway(1500); 
            }
        })
  },
  async updateVehicle({commit}, editVehicle){
    const editVehicleObject = editVehicle.editVehicle;
    await this.$axios.$put(API_BASE_PATH +'vehicle/' + editVehicle.vehicleId, {
            ...editVehicleObject
          })
          .then(res => {
              const vehicleRes = res != null ? res : null;
              if(vehicleRes != null){
                commit('updateVehicle', vehicleRes);
                this.$toast.success('Vehicle Updated').goAway(1500); 
              }
          })
  },
  async deleteVehicle({commit}, vehicleId){
    await this.$axios.$delete(API_BASE_PATH +'vehicle/' + vehicleId, {})
          .then(res => {
              commit('deleteVehicle', vehicleId);
              this.$toast.success('Vehicle Deleted').goAway(1500); 
              // const vehicleRes = res.data != null ? res.data : null;
              // if(vehicleRes != null){
              //   commit('deleteVehicle', vehicleId);
              //   this.$toast.success('Vehicle Deleted').goAway(1500); 
              // }
          })
  },

  
  //Image Uploader
  async saveImage({commit}, newCostomer){
    await this.$axios.$post('/api/upload-files', {
             ...newCostomer
          }, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
          })
          .then(res => {
            commit('addNewCustomer', res);
              const customerRes = res.data != null ? res.data : null;
              if(customerRes != null){
                commit('addNewCustomer', customerRes);
              }
          })
  },


  //Category Actions
  async getCategories({state,commit}){
        // if(state.customers.length) return;
        await this.$axios.$get(API_BASE_PATH +'category/list')
          .then(res => {
              const categoryRes = res.categories != null ? res.categories : [];
              if(categoryRes.length > 0){
                commit('resetCategories', []);
                categoryRes.forEach(item => {
                  console.log("item",item);
                  commit('setCategories', item);
                })
              }
          })
    },

  async saveCategory({commit}, newCategory){
  await this.$axios.$post(API_BASE_PATH +  'category', {
          ...newCategory
        })
        .then(res => {
            const categoryRes = res != null ? res : null;
            if(categoryRes != null){
              commit('addNewCategory', categoryRes);
              this.$toast.success('Category Added').goAway(1500); 
            }
        })
  },
  async updateCategory({commit}, editCategory,){
    const editCategoryObject = editCategory.editCategory;
    await this.$axios.$put( API_BASE_PATH +  'category/' + editCategory.categoryId, {
            ...editCategoryObject
          })
          .then(res => {
              const CategoryRes = res != null ? res : null;
              if(CategoryRes != null){
                commit('updateCategory', CategoryRes);
                this.$toast.success('Category Updated').goAway(1500);
              }
          })
  },
  async deleteCategory({commit}, categoryId){
    await this.$axios.$delete(API_BASE_PATH +  'category/' + categoryId, {})
          .then(res => {
              const categoryRes = res != null ? res : null;
              if(categoryRes != null){
                commit('deleteCategory', categoryId);
                this.$toast.success('Category Deleted').goAway(1500);
              }
          })
  },

   //Inventory Actions
   async getInventory({state,commit}, options){
        const { page, itemsPerPage, sortBy, sortDesc } = options.options;
        const search = options.search;
        const sort = sortDesc[0] ? 'DESC' : 'ASC'
        const pageNumber = page - 1;
        const sortByValue = sortBy.length > 0 ? sortBy[0] : "id";
        console.log("page", page);
        commit('resetInventory', []);
        commit('resetPagination', []);
        await this.$axios.$get(API_BASE_PATH +'inventory/grid?pageNumber='+ pageNumber +'&sortBy='+ sortByValue +'&sortDirection='+ sort +'&pageSize=' + itemsPerPage)
          .then(res => {
              const inventoryRes = res.content != null ? res.content : [];
              const inventoryCount = res.size != null ? res.size : 0;
              const numberOfPages = Math.round(inventoryCount/ itemsPerPage);
              commit('setTotalItems', inventoryCount);
              commit('setNumberOfPages', numberOfPages )
              if(inventoryRes.length > 0){              
                inventoryRes.forEach(item => {
                  commit('setInventory', item);
                })
              }
          })


    },
     async getInventoryList({state,commit}){
        commit('resetInventory', []);
        await this.$axios.$get(API_BASE_PATH +'inventory/list')
          .then(res => {
              const categoryRes = res.inventories != null ? res.inventories : [];
              if(categoryRes.length > 0){
                categoryRes.forEach(item => {
                  commit('setInventory', item);
                })
              }
          })
    },

  async saveInventory({commit}, newInventory){
    await this.$axios.$post(API_BASE_PATH + 'inventory/', {
          ...newInventory
        })
        .then(res => {
            const inventoryRes = res != null ? res : null;
            if(inventoryRes != null){
              commit('addNewInventory', inventoryRes);
              this.$toast.success('Inventory Added').goAway(1500);
            }
        })
  },
  async updateInventory({commit}, editInventory,){
    const editInventoryObject = editInventory.editInventory;
    await this.$axios.$put(API_BASE_PATH +'inventory/' + editInventory.inventoryId, {
            ...editInventoryObject
          })
          .then(res => {
              const inventoryRes = res != null ? res : null;
              if(inventoryRes != null){
                commit('updateInventory', inventoryRes);
                this.$toast.success('Inventory Updated').goAway(1500);
              }
          })
  },
  async deleteInventory({commit}, inventoryId){
    await this.$axios.$delete(API_BASE_PATH +'inventory/' + inventoryId, {})
          .then(res => {
                commit('deleteInventory', inventoryId);
                this.$toast.success('Inventory Deleted').goAway(1500);
              // const inventoryRes = res.data != null ? res.data : null;
              // if(inventoryRes != null){
              //   commit('deleteInventory', inventoryId);
              //   this.$toast.success('Inventory Deleted').goAway(1500);
              // }
          })
  },
   //Service Actions
    async getService({state,commit}, options){
        const { page, itemsPerPage, sortBy, sortDesc } = options.options;
        const search = options.search;
        const sort = sortDesc[0] ? 'DESC' : 'ASC'
        const pageNumber = page - 1;
        const sortByValue = sortBy.length > 0 ? sortBy[0] : "id";
        commit('resetService', []);
        commit('resetPagination', []);
        await this.$axios.$get(API_BASE_PATH + 'service/grid?pageNumber='+ pageNumber +'&sortBy='+ sortByValue +'&sortDirection='+ sort +'&pageSize=' + itemsPerPage)
          .then(res => {
              const serviceRes = res.content != null ? res.content : [];
              const serviceCount = res.size != null ? res.size : 0;
              const numberOfPages = Math.round(serviceCount/ itemsPerPage);
              commit('setTotalItems', serviceCount);
              commit('setNumberOfPages', numberOfPages );
              if(serviceRes.length > 0){              
                serviceRes.forEach(item => {
                  commit('setService', item);
                })
              }
          })
    },
  async getServiceList({state,commit}){
        // if(state.customers.length) return;
        await this.$axios.$get(API_BASE_PATH + 'service/list')
          .then(res => {
              const serviceRes = res.services != null ? res.services : [];
              if(serviceRes.length > 0){
                commit('resetService', []);
                serviceRes.forEach(item => {
                  commit('setService', item);
                })
              }
          })
    },

  async saveService({commit}, newService){
    await this.$axios.$post(API_BASE_PATH + 'service', {
          ...newService
        })
        .then(res => {
            const serviceRes = res != null ? res : null;
            if(serviceRes != null){
              commit('addNewService', serviceRes);
              this.$toast.success('Service Added').goAway(1500);
            }
        })
  },
  async updateService({commit}, editService,){
    const editServiceObject = editService.editService;
    await this.$axios.$put(API_BASE_PATH + 'service/' + editService.serviceId, {
            ...editServiceObject
          })
          .then(res => {
              const serviceRes = res != null ? res : null;
              if(serviceRes != null){
                commit('updateService', serviceRes);
                this.$toast.success('Service Updated').goAway(1500);
              }
          })
  },
  async deleteService({commit}, serviceId){
    await this.$axios.$delete(API_BASE_PATH + 'service/' + serviceId, {})
          .then(res => {
              const serviceRes = res != null ? res : null;
              if(serviceRes != null){
                commit('deleteService', serviceId);
                this.$toast.success('Service Deleted').goAway(1500);
              }
          })
  },
   //Technicians Actions
    async getTechnicians({state,commit}, options){
        const { page, itemsPerPage, sortBy, sortDesc } = options.options;
        const search = options.search;
        const sort = sortDesc[0] ? 'DESC' : 'ASC'
        const pageNumber = page - 1;
        const sortByValue = sortBy.length > 0 ? sortBy[0] : "id";
        commit('resetTechnician', []);
        commit('resetPagination', []);
        await this.$axios.$get(API_BASE_PATH + 'technician/grid?pageNumber='+ pageNumber +'&sortBy='+ sortByValue +'&sortDirection='+ sort +'&pageSize=' + itemsPerPage)
          .then(res => {
              const technicianRes = res.content != null ? res.content : [];
              const technicianCount = res.size != null ? res.size : 0;
              const numberOfPages = Math.round(technicianCount/ itemsPerPage);
              commit('setTotalItems', technicianCount);
              commit('setNumberOfPages', numberOfPages );
              if(technicianRes.length > 0){              
                technicianRes.forEach(item => {
                  commit('setTechnician', item);
                })
              }
          })
    },
   async getTechnicianList({state,commit}){
        // if(state.customers.length) return;
        await this.$axios.$get(API_BASE_PATH +'technician/list')
          .then(res => {
              const technicianRes = res.technicians != null ? res.technicians : [];
              if(technicianRes.length > 0){
                commit('resetTechnician', []);
                technicianRes.forEach(item => {
                  commit('setTechnician', item);
                })
              }
          })
    },

  async saveTechnician({commit}, newTechnician){
    await this.$axios.$post(API_BASE_PATH + 'technician', {
          ...newTechnician
        })
        .then(res => {
            const technicianRes = res != null ? res : null;
            if(technicianRes != null){
              commit('addNewTechnician', technicianRes);
              this.$toast.success('Technician Added').goAway(1500);
            }
        })
  },
  async updateTechnician({commit}, editTechnician,){
    const editTechnicianObject = editTechnician.editTechnician;
    await this.$axios.$put(API_BASE_PATH + 'technician/' + editTechnician.technicianId, {
            ...editTechnicianObject
          })
          .then(res => {
              const technicianRes = res != null ? res : null;
              if(technicianRes != null){
                commit('updateTechnician', technicianRes);
                this.$toast.success('Technician Updated').goAway(1500);
              }
          })
  },
  async deleteTechnician({commit}, technicianId){
    await this.$axios.$delete(API_BASE_PATH + 'technician/' + technicianId, {})
          .then(res => {
              const technicianRes = res != null ? res : null;
              if(technicianRes != null){
                commit('deleteTechnician', technicianId);
                this.$toast.success('Technician Deleted').goAway(1500)
              }
          })
  },
   //Job Actions
    async getJobs({state,commit}, options){
        const { page, itemsPerPage, sortBy, sortDesc } = options.options;
        const search = options.search;
        const startDate = options.startDate ? options.startDate : "";
        const endDate = options.endDate ? options.endDate : "";
        const sort = sortDesc[0] ? 'desc' : 'asc'
        const pageNumber = page != 0 ? page : page + 1;
        commit('resetPagination', []);
        commit('resetJobs', []);
        commit('resetJobId', []);
        commit('resetSpare', []);
        commit('resetSpareTotal', []);
        commit('resetJobService', []);
        commit('resetJobServiceTotal', []);
        commit('resetDiscount', []);
        await this.$axios.$get('/api/job_card/grid?page='+ pageNumber +'&filter='+search+'&sortBy='+ sortBy +'&sort='+ sort +'&page_size=' + itemsPerPage + '&startDate='+startDate+'&endDate='+endDate)
          .then(res => {
              const jobsRes = res.data.results != null ? res.data.results : [];
              const jobsCount = res.data.count != null ? res.data.count : 0;
              const numberOfPages = Math.round(jobsCount/ itemsPerPage);
              commit('setTotalItems', jobsCount);
              commit('setNumberOfPages', numberOfPages );
              if(jobsRes.length > 0){              
                jobsRes.forEach(item => {
                  let i = {
                    ...item,
                    first_name: item.customer.first_name +' '+ item.customer.last_name,
                    customer: item.customer.id,
                    vehicle_registry: item.vehicle_registry.vehicle_number,
                    past_job: item.past_job != null ? item.past_job.job_id : '',
                    next_service_at: item.next_service_at != null ? item.next_service_at.split('T')[0] : '',
                    mobile_number: item.customer.mobile_number,
                    past_next_service_at: item.past_job != null ? item.past_job.next_service_at.split('T')[0] : '',
                  }
                  commit('setJobs', i);
                })
              }
          })
    },

    async getJobList({state,commit}){
        // if(state.customers.length) return;
        // await this.$axios.$get('/api/job_card/list')
        //   .then(res => {
        //       const jobRes = res.data != null ? res.data : [];
        //       if(jobRes.length > 0){
        //         commit('resetJobs', []);
        //         jobRes.forEach(item => {
        //           commit('setJobs', item);
        //         })
        //       }
        //   })
    },

    async setJobId({commit}, jobId){
      commit('setJobId', jobId);
    },

    async setDiscount({commit}, discount){
      commit('setDiscount', discount);
    },

    async saveJob({commit}, jobObject){
      const job = jobObject.jobObject;
      await this.$axios.$post( API_BASE_PATH +'job', {
          ...job
        })
        .then(res => {
            const jobRes = res != null ? res : null;
            if(jobRes != null){
              commit('setJobId', jobRes.id);
              this.$toast.success('Job Added').goAway(1500);
            }
        })
    },

    async updateJob({commit}, editJob,){
      const editJobObject = editJob.jobObject;
      await this.$axios.$put('/api/job_card/update/' + editJob.id, {
              ...editJobObject
            })
            .then(res => {
                const jobRes = res.data != null ? res.data : null;
                if(jobRes != null){
                  this.$toast.success('Job Updated').goAway(1500);
                }
            })
    },

    async deleteJob({commit}, jobId){
    await this.$axios.$put('/api/job_card/delete/' + jobId, {})
          .then(res => {
              const jobRes = res.data != null ? res.data : null;
              if(jobRes != null){
                commit('deleteJob', jobId);
                this.$toast.success('Job Deleted').goAway(1500);              }
          })
  },

  //Spare Actions
  async getSpares({state,commit}, jobId){
        // if(state.customers.length) return;
        await this.$axios.$get('/api/job_card/spare-list/' + jobId)
          .then(res => {
              const spareRes = res.data != null ? res.data : [];
              if(spareRes.length > 0){
                commit('resetSpare', []);
                spareRes.forEach(item => {
                  let i = {
                    ...item,
                    inventory: item.inventory.id,
                    part_name: item.inventory.part_name,
                    part_number: item.inventory.part_number,
                    sale_price: item.inventory.sale_price,
                    total : parseFloat(item.inventory.sale_price) * parseInt(item.quantity)
                  }
                  commit('setSpare', i);
                })
              }
              commit('calculateSparesTotal');
          })
    },

  async saveSpare({commit}, newSpare){
    const jobId = newSpare.jobId;
    const spareObject = newSpare.editSpare
    await this.$axios.$post('/api/job_card/spare-add-one-by-one/'+ jobId, {
          ...spareObject
        })
        .then(res => {
            const spareRes = res.data != null ? res.data : null;
            if(spareRes != null){
              this.$toast.success('Spare Item Added').goAway(1500); 
            }
        })
  },
  async updateSpare({commit}, newSpare){
    const jobId = newSpare.jobId;
    const spareObject = newSpare.editSpare;
    const spareId = newSpare.id;
    await this.$axios.$put('/api/job_card/spare-update/'+ spareId, {
          ...spareObject
        })
        .then(res => {
            const spareRes = res.data != null ? res.data : null;
            if(spareRes != null){
              this.$toast.success('Spare Item Update').goAway(1500); 
            }
        })
  },
  async deleteSpare({commit}, spareId){
    await this.$axios.$put('/api/job_card/spare-delete/'+ spareId, {})
        .then(res => {
            const spareRes = res.data != null ? res.data : null;
            if(spareRes != null){
              this.$toast.success('Spare Item Deleted').goAway(1500); 
            }
        })
  },

  //Job Service Actions
  async getJobService({state,commit}, jobId){
    await this.$axios.$get('/api/job_card/service-list/' + jobId)
      .then(res => {
          const serviceRes = res.data != null ? res.data : [];
          if(serviceRes.length > 0){
            commit('resetJobService', []);
            serviceRes.forEach(item => {
              let i = {
                ...item,
                service_registry: item.service_registry.id,
                service_name: item.service_registry.service_name,
                service_charge: item.service_registry.service_charge,
                technician: item.technician.id,
                technician_first_name: item.technician.technician_first_name +' '+ item.technician.technician_last_name,
                total : parseFloat(item.service_registry.service_charge) * parseInt(item.quantity)
              }
              commit('setJobService', i);
            })
          }
          commit('calculateJobServiceTotal');
      })
    },
    async saveJobService({commit}, newJobService){
    const jobId = newJobService.jobId;
    const jobServiceObject = newJobService.editJobService
    await this.$axios.$post('/api/job_card/service-add-one-by-one/'+ jobId, {
          ...jobServiceObject
        })
        .then(res => {
            const jobServiceRes = res.data != null ? res.data : null;
            if(jobServiceRes != null){
              this.$toast.success('Service Added').goAway(1500); 
            }
        })
  },
  async updateJobService({commit}, newJobService){
    const jobId = newJobService.jobId;
    const jobServiceObject = newJobService.editJobService;
    const jobServiceId = newJobService.id;
    await this.$axios.$put('/api/job_card/service-update/'+ jobServiceId, {
          ...jobServiceObject
        })
        .then(res => {
            const jobServiceRes = res.data != null ? res.data : null;
            if(jobServiceRes != null){
              this.$toast.success('Service Update').goAway(1500); 
            }
        })
  },
  async deleteJobService({commit}, jobServiceId){
    await this.$axios.$put('/api/job_card/service-delete/'+ jobServiceId, {})
        .then(res => {
            const jobServiceRes = res.data != null ? res.data : null;
            if(jobServiceRes != null){
              this.$toast.success('Service item Deleted').goAway(1500); 
            }
        })
  },

  //discount Actions
  async updateDiscount({commit}, discountObject){
    const jobId = discountObject.jobId;
    const discount = discountObject.discount;
    await this.$axios.$put('/api/job_card/update-discount/'+ jobId, {
          discount
        })
        .then(res => {
            const jobDiscount = res.data != null ? res.data : null;
            if(jobDiscount != null){
              // this.$toast.success('').goAway(1500); 
            }
        })
  },


}
