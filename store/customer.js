export const state = () => ({
    customers: []
})

export const getters = {
    getterValue: state => {
        return state.value
    }
}

export const mutations = {
    updateCustomers: (state, payload) => {
        state.customers = payload
    },
    updateValue: (state, payload) => {
        state.value = payload
    }
}

export const actions = {
    updateActionValue({ commit }) {
        commit('updateValue', payload)
    },
    async getCustomers({commit}){
        console.log('getCustomer');
        await this.axios.get('/api/customer/grid?page=1&filter&sortBy&sort=asc')
        .then(res => {
            console.log('customer.res', res);
        })
        // try{
        // } catch(err) {
        //     console.log(err);
        // }
    }
}