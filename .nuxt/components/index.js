export { default as DataTable } from '../..\\components\\DataTable.vue'
export { default as ImageUploader } from '../..\\components\\ImageUploader.vue'
export { default as Job } from '../..\\components\\Job.vue'
export { default as JobGrid } from '../..\\components\\JobGrid.vue'
export { default as JobTotal } from '../..\\components\\JobTotal.vue'
export { default as Logo } from '../..\\components\\Logo.vue'
export { default as Services } from '../..\\components\\Services.vue'
export { default as SpareParts } from '../..\\components\\SpareParts.vue'
export { default as VuetifyLogo } from '../..\\components\\VuetifyLogo.vue'

export const LazyDataTable = import('../..\\components\\DataTable.vue' /* webpackChunkName: "components_DataTable" */).then(c => c.default || c)
export const LazyImageUploader = import('../..\\components\\ImageUploader.vue' /* webpackChunkName: "components_ImageUploader" */).then(c => c.default || c)
export const LazyJob = import('../..\\components\\Job.vue' /* webpackChunkName: "components_Job" */).then(c => c.default || c)
export const LazyJobGrid = import('../..\\components\\JobGrid.vue' /* webpackChunkName: "components_JobGrid" */).then(c => c.default || c)
export const LazyJobTotal = import('../..\\components\\JobTotal.vue' /* webpackChunkName: "components_JobTotal" */).then(c => c.default || c)
export const LazyLogo = import('../..\\components\\Logo.vue' /* webpackChunkName: "components_Logo" */).then(c => c.default || c)
export const LazyServices = import('../..\\components\\Services.vue' /* webpackChunkName: "components_Services" */).then(c => c.default || c)
export const LazySpareParts = import('../..\\components\\SpareParts.vue' /* webpackChunkName: "components_SpareParts" */).then(c => c.default || c)
export const LazyVuetifyLogo = import('../..\\components\\VuetifyLogo.vue' /* webpackChunkName: "components_VuetifyLogo" */).then(c => c.default || c)
