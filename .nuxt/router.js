import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _4b1eb924 = () => interopDefault(import('..\\pages\\customer.vue' /* webpackChunkName: "pages/customer" */))
const _aa9bd5f8 = () => interopDefault(import('..\\pages\\inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _627aa2da = () => interopDefault(import('..\\pages\\login.vue' /* webpackChunkName: "pages/login" */))
const _b313f942 = () => interopDefault(import('..\\pages\\service.vue' /* webpackChunkName: "pages/service" */))
const _054d1c2a = () => interopDefault(import('..\\pages\\technician.vue' /* webpackChunkName: "pages/technician" */))
const _4afb7785 = () => interopDefault(import('..\\pages\\upcomming.vue' /* webpackChunkName: "pages/upcomming" */))
const _25feb856 = () => interopDefault(import('..\\pages\\vehicle.vue' /* webpackChunkName: "pages/vehicle" */))
const _2f9bb604 = () => interopDefault(import('..\\pages\\inventory\\category.vue' /* webpackChunkName: "pages/inventory/category" */))
const _ae2df168 = () => interopDefault(import('..\\pages\\inventory\\grid.vue' /* webpackChunkName: "pages/inventory/grid" */))
const _799e4b18 = () => interopDefault(import('..\\pages\\jobs\\create.vue' /* webpackChunkName: "pages/jobs/create" */))
const _2b6f0d7e = () => interopDefault(import('..\\pages\\jobs\\view.vue' /* webpackChunkName: "pages/jobs/view" */))
const _3da08504 = () => interopDefault(import('..\\pages\\jobs\\edit\\_id.vue' /* webpackChunkName: "pages/jobs/edit/_id" */))
const _051b0b7c = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/customer",
    component: _4b1eb924,
    name: "customer"
  }, {
    path: "/inspire",
    component: _aa9bd5f8,
    name: "inspire"
  }, {
    path: "/login",
    component: _627aa2da,
    name: "login"
  }, {
    path: "/service",
    component: _b313f942,
    name: "service"
  }, {
    path: "/technician",
    component: _054d1c2a,
    name: "technician"
  }, {
    path: "/upcomming",
    component: _4afb7785,
    name: "upcomming"
  }, {
    path: "/vehicle",
    component: _25feb856,
    name: "vehicle"
  }, {
    path: "/inventory/category",
    component: _2f9bb604,
    name: "inventory-category"
  }, {
    path: "/inventory/grid",
    component: _ae2df168,
    name: "inventory-grid"
  }, {
    path: "/jobs/create",
    component: _799e4b18,
    name: "jobs-create"
  }, {
    path: "/jobs/view",
    component: _2b6f0d7e,
    name: "jobs-view"
  }, {
    path: "/jobs/edit/:id?",
    component: _3da08504,
    name: "jobs-edit-id"
  }, {
    path: "/",
    component: _051b0b7c,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
